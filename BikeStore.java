package Java.Labs.Lab_2.fall2021lab02;

/*
* Ray Hernaez
* 1241417
*/

public class BikeStore {
    public static void main(String[] args) {

        Bicycle[] bikes = new Bicycle[4];
        
        bikes[0] = new Bicycle("Canyon Aeroad CF SLX", 10, 15.7);
        bikes[1] = new Bicycle("Ridley Noah SL", 18, 15.6);
        bikes[2] = new Bicycle("Sensa Romagna", 12, 15.5);
        bikes[3] = new Bicycle("Scott Foil 10", 15, 15.4);

        for (int i = 0; i <bikes.length; i++) {
            System.out.println(bikes[i]);
        }
    }
}