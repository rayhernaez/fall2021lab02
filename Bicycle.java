package Java.Labs.Lab_2.fall2021lab02;

/*
* Ray Hernaez
* 1241417
*/

public class Bicycle {

    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    //constructor
    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    //getters
    public String getManufacturer() {
        return this.manufacturer;
    }
    public int getNumberGears() {
        return this.numberGears;
    }
    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    //toString
    public String toString() {
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: " + this.maxSpeed;
    }
}